/*
*                   Bài 1
*
* Input: lương 1 ngày:100000 và số ngày làm:30 ngày
*
* Step:
* tạo biến lương 1 ngày và số ngày làm
* tạo biến chứa kết quả cần tìm
* áp dụng công thức lương = lương 1 ngày * số ngày làm
*
* Output: 3000000
*/
var luong1 = 100000;
var songay = 30;
var luongtong = null;

luongtong = luong1 * songay;
console.log("bài 1");
console.log('luongtong: ', luongtong);

/*
*                   Bài 2
*
* Input: 5 số thực: 1,2,3,4,5
*
* Step:
* tạo 5 biến lần lượt là: a,b,c,d,e
* tạo biến trung bình 
* áp dụng công thức: trungbinh =(a+b+c+d+e)/5
*
* Output: 3
*/
var a = 1;
var b = 2;
var c = 3;
var d = 4;
var e = 5;
var trungbinh = null;
trungbinh = (a + b + c + d + e) / 5;
console.log("bài 2");
console.log('trungbinh: ', trungbinh);

/*
*                   Bài 3
*
* Input: tiền đô: 100, tiền việt: 23.500
*
* Step:
* tạo 1 biến chứa kết quả cần tìm
* áp dụng công thức usdtovn = usd *vn
*
* Output: 2350000
*/
var vn = 23500;
var usd = 100;
var usdtovn = null;
usdtovn = usd * vn;
console.log("bai 3");
console.log('usdtovn: ', usdtovn);

/*
*                   Bài 4
*
* Input: chiều dài:30, chiều rộng:20
*
* Step:
* tạo 2 biến chiều dài chiều rộng
* tạo biến chưa kết quả cần tìm
* áp dụng công thức: CV =(dai + rong)*2, DT = dai * rong
*
* Output: chuvi:100, dientich:600
*/
var dai = 30;
var rong = 20;
var chuvi = null;
var dientich = null;
console.log("bai 4");
chuvi = (dai + rong) * 2;
console.log('chuvi: ', chuvi);
dientich = dai * rong;
console.log('dientich: ', dientich);

/*
*                   Bài 5
*
* Input: number1: 12, number2: 44
*
* Step: 
* tạo 4 biến donvi và chuc
* tạo 2 biến chứa kết quả
* áp dụng công thức donvi = number % 10, chuc = number / 10,
* tổng = donvi + chuc vu
* 
* Output: tong1: 3, tong2: 8
*/
var number1 = 12;
var number2 = 44;
console.log("bai 5");
var donvi1 = number1 % 10;
var chuc1 = Math.floor(number1 / 10);
var tong1 = donvi1 + chuc1;
console.log('tong1: ', tong1);
var donvi2 = number2 % 10;
var chuc2 = Math.floor(number2 / 10);
var tong2 = donvi2 + chuc2;
console.log('tong2: ', tong2);
